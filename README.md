# Procedural Fcurve Editor

An add-on for blender that adds a few tools to make work with areas of animation curves a little easier.

[Download](https://gitlab.com/bird_d/procedural-fcurve-editor/-/releases)

[Wiki](https://gitlab.com/bird_d/procedural-fcurve-editor/-/wikis/home)

# Todo
- [ ] Restructure code in to different files for readability.
- [x] Add shortcuts to keymap on add-on load.
- [x] Add a function to map the current time selection to a modifier.
- [ ] Add videos that show off what this does.
- [ ] Add better comments after splitting files.
- [ ] Add more fcurve manipulating modal operators.
- [ ] See about making a request to blender to enable gizmos for graph editor? Probably asking too much, but would make it so I don't have to use a modal for moving handles.

# Current Fcurve modifying operators
- [x] Cursor
- [x] Playhead
- [x] Zeroing
- [x] Noise
- [x] Weighted Moving Average Smooth
- [x] Ramp
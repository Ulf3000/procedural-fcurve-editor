	# Procedural Fcurve Editor, Adds tools to make modifying fcurves easier.
	# Copyright (C) 2020 bird_d <relay198@gmail.com>

	# This program is free software: you can redistribute it and/or modify
	# it under the terms of the GNU General Public License as published by
	# the Free Software Foundation, either version 3 of the License, or
	# (at your option) any later version.

	# This program is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU General Public License for more details.

	# You should have received a copy of the GNU General Public License
	# along with this program.  If not, see <https://www.gnu.org/licenses/>.

bl_info = {
	"name": "Procedural Fcurves",
	"author": "bird_d",
	"version": (0, 1, 4),
	"blender": (2, 83, 0),
	"location": "Graph Editor > F3 > 'Procedural'",
	"description": "Some stuff for procedural modification of FCurves",
	"warning": "",
	"doc_url": "https://gitlab.com/bird_d/procedural-fcurve-editor/-/wikis/home",
	"category": "Animation",
}

import bpy
import gpu
#import math
import random
from gpu_extras.batch import batch_for_shader
import bgl
import blf
from bgl import *
from bpy.props import IntProperty, FloatProperty, BoolProperty, EnumProperty, StringProperty

#region    #Common/Helper Functions#
def calc_weight_three_point(a, b, d):
	return a*0.2 + b*0.3 + c*0.5
def calc_weight_five_point(a, b, c, d, e):
	return (a*0.1 + b*0.25 + c*0.5 + d*0.25 + e*0.1) / 1.2
def clamp(val, lower=0, upper=1):
	return lower if val < lower else upper if val > upper else val
def get_active_fcurves(context):
	return [fc for fc in context.active_object.animation_data.action.fcurves if fc.select]
def normalize(val, min_val, max_val):
	if min_val == max_val:
		return val
	return ((val - min_val) / (max_val - min_val))
def scale(scale, min_val, max_val):
	return (scale * (max_val - min_val)) + min_val

##TODO: Improve common handling of fcurves
def handle_common_errors(self):
	if bpy.context.area.type != 'GRAPH_EDITOR':
		self.report({'WARNING'}, "Somehow didn't run this in graph editor area")
		return False
	pg = bpy.types.Scene.procedural_graph_handler
	if pg.start == pg.end:
		self.report({'WARNING'}, "No area selected to modify.")
		return False
	if len(self.fcurves) == 0:
		self.report({'WARNING'}, "Must have Fcurve channel(s) selected.")
		return False
	return True

def setup_fcurve_restore(self, fcurve):
	pg = bpy.types.Scene.procedural_graph_handler
	kp_arr = []
	for kp in fcurve.keyframe_points:
		if pg.start <= kp.co[0] <= pg.end:
			kp_arr.append((kp.co[0], kp.co[1]))
	self.Hfcurves.append(kp_arr)

def restore_fcurves(self):
	#self.fcurves should probably just be changed to use anim_data.fcurves.
	for i in range(0, len(self.fcurves)):
		fc = self.fcurves[i]
		pg = bpy.types.Scene.procedural_graph_handler
		for kp_i in range(pg.start, pg.end+1):
			keyframe = None
			for kp in fc.keyframe_points:
				if kp.co[0] == kp_i:
					keyframe = kp
			fc.keyframe_points.remove(keyframe, fast=True)
		for co in self.Hfcurves[i]:
			fc.keyframe_points.insert(frame=co[0], value=co[1], options={"NEEDED"})
##

def draw_box_from_points(x=0, y=0, x2=200, y2=100, col=(1, 0, 0.75, 1.0)):
	shader = gpu.shader.from_builtin('2D_UNIFORM_COLOR')
	
	bgl.glEnable(bgl.GL_BLEND)

	#print("x: ", x, ", x2: ", x2)
	verts = [(x,y), (x2,y), (x2,y2), (x,y2)]
	faces = [ verts[0], verts[1], verts[2], verts[3], verts[2], verts[0] ]
	batch = batch_for_shader(shader, 'TRIS', {"pos": faces})
	shader.bind()
	shader.uniform_float("color", col)
	batch.draw(shader)
	
	#Defaults
	bgl.glDisable(bgl.GL_BLEND)

def draw_selection_handle(self, context):
	x,tmp = context.region.view2d.view_to_region(self.hover_frame, 0, clip=False)
	draw_box_from_points(x-2, 11, x+2, context.area.height-49, (1, 0.75, 0.5, 0.85))

def draw_gradient_box_from_points(x=0, y=0, x2=200, y2=100, col=(1, 0, 0.75, 1.0), col2=(1,0,0,1)):
	shader = gpu.shader.from_builtin('2D_SMOOTH_COLOR')
	
	bgl.glEnable(bgl.GL_BLEND)

	verts = [(x,y), (x2,y), (x2,y2), (x,y2)]
	faces = [ verts[0], verts[1], verts[2], verts[0], verts[2], verts[3] ]
	vcols = [col2, col, col, col2, col, col2]
	batch = batch_for_shader(shader, 'TRIS', {"pos": faces, "color": vcols})
	shader.bind()
	batch.draw(shader)
	
	#Defaults
	bgl.glDisable(bgl.GL_BLEND) 
#endregion #Common/Helper Functions#
class Procedural_Graph_Handler():
	def __init__(self):
		self.start = 0
		self.end = 0
		self.falloff_start = 0
		self.frame_start = 0#IntProperty(default=0) ?
		self.frame_end = 0
		self.falloff_end = 0
		self.enabled = False
		self.editing = BoolProperty(default=True)
	
	def draw(self):
		if self.start == self.end:
			return
		area = bpy.context.area
		fo_start_x,tmp = bpy.context.region.view2d.view_to_region(self.falloff_start, 0, clip=False)
		x1,tmp = bpy.context.region.view2d.view_to_region(self.frame_start, 0, clip=False)
		x2,tmp = bpy.context.region.view2d.view_to_region(self.frame_end, 0, clip=False)
		fo_end_x,tmp = bpy.context.region.view2d.view_to_region(self.falloff_end, 0, clip=False)
		col = (0.33, 0.75, 0.33, 0.33)
		col2 = (0.33, 0.75, 0.33, 0.15)
		handle_col = (0, 0, 0, 0.75)
		y1 = 11
		y2 = area.height-49
		if self.falloff_start < self.frame_start:
			draw_gradient_box_from_points(fo_start_x, y1, x1, y2, col, col2)
			draw_box_from_points(fo_start_x, y1, fo_start_x+1, y2, handle_col)
		draw_box_from_points(x1, y1, x2, y2, col)
		if self.falloff_end > self.frame_end:
			draw_gradient_box_from_points(x2, y1, fo_end_x, y2, col2, col)
			draw_box_from_points(fo_end_x, y1, fo_end_x+1, y2, handle_col)
		#handles

		draw_box_from_points(x1, y1, x1+1, y2, handle_col)
		draw_box_from_points(x2, y1, x2+1, y2, handle_col)

		#draw_selection_area(self.frame_start, self.frame_end)
	def enable(self):
		print("PGHandler Enabled")
		self._handle = bpy.types.SpaceGraphEditor.draw_handler_add(
			self.draw, (), 
			'WINDOW', 
			'POST_PIXEL')
		self.enabled = True
		for area in bpy.context.screen.areas:
			area.tag_redraw()
	def disable(self):
		print("PGHandler Disabled")
		if self.enabled == False:
			return
		bpy.types.SpaceGraphEditor.draw_handler_remove(self._handle, "WINDOW")
		self.enabled = False
		self.falloff_start = 0
		self.frame_start = 0
		self.frame_end = 0
		self.falloff_end = 0
		self.update_range()
		for area in bpy.context.screen.areas:
			area.tag_redraw()
	def check(self):
		if self.frame_start == self.frame_end:
			return True
		return None
	def get_frame_influence(self, frame):
		if frame >= self.frame_start and frame <= self.frame_end:
			if frame == self.start or frame == self.end:
				return 0
			return 1
		if frame < self.frame_start:
			if frame > self.falloff_start:
				return normalize(frame, self.falloff_start, self.frame_start)
		elif frame > self.frame_end:
			if frame < self.falloff_end:
				return normalize(frame, self.falloff_end, self.frame_end)
		return 0
	def update_range(self):
		if self.falloff_start > self.frame_start:
			self.falloff_start = self.frame_start
		if self.falloff_end < self.frame_end:
			self.falloff_end = self.frame_end
		self.start = self.frame_start
		if self.falloff_start < self.frame_start:
			self.start = self.falloff_start
		self.end = self.frame_end
		if self.falloff_end > self.frame_end:
			self.end = self.falloff_end

#region    #RANGE MANIPULATION OPERATORS#
class GRAPH_OT_toggle_proceduralrange_mode(bpy.types.Operator):
	pass #Need gizmos activated in Graph editor before this can be implemented

class GRAPH_OT_proceduralrange_set(bpy.types.Operator):
	"""Set the range for procedural animation"""
	bl_idname = "graph.proceduralrange_set"
	bl_label = "Set Procedural Range"

	mouse_x_start: IntProperty()
	hold_falloff_start: IntProperty()
	hold_start: IntProperty()
	hold_end: IntProperty()
	hold_falloff_start: IntProperty()
	pressing = False
	hover_frame = 0
	
	def modal(self, context, event):
		area = bpy.context.area
		area.tag_redraw()
		pg = bpy.types.Scene.procedural_graph_handler
		#Get the right region, else it could run calculations on the wrong region inside the area
		region = None
		for r in area.regions:
			if r.type == "WINDOW":
				region = r
				break
		self.hover_frame,tmp = region.view2d.region_to_view(event.mouse_x - region.x, 0)
		self.hover_frame = round(self.hover_frame)
		if event.type == 'MOUSEMOVE':
			if self.pressing:
				##HACK: There has to be a better way to do this, since y is unused. 
				##  	May not need to tho, if add decorating lines.
				#minus by region's x to correct for the channel panel
				tmpx,tmpy = region.view2d.region_to_view(event.mouse_x - region.x, 0)
				delta = round((tmpx - self.mouse_x_start)) 
				if delta < 0:
					pg.frame_start = round(tmpx)
					pg.frame_end = self.mouse_x_start
				else:
					pg.frame_start = self.mouse_x_start
					pg.frame_end = round(tmpx)
				pg.falloff_start = pg.frame_start
				pg.falloff_end = pg.frame_end
				pg.update_range()
		elif event.type == 'LEFTMOUSE':
			if event.value == "PRESS":
				self.pressing = True
				tmpx, tmp = region.view2d.region_to_view(event.mouse_x - region.x, 0)
				self.mouse_x_start = round(tmpx)
				pg.frame_start = pg.frame_end = self.mouse_x_start
				pg.falloff_start = pg.falloff_end = self.mouse_x_start
				pg.update_range()
			elif self.pressing == True and event.value == "RELEASE":
				bpy.types.SpaceGraphEditor.draw_handler_remove(self._handle, 'WINDOW')
				context.window.cursor_modal_restore()
				pg.update_range()
				return {'FINISHED'}

		elif event.type in {'RIGHTMOUSE', 'ESC'}:
			pg.falloff_start = self.hold_falloff_start
			pg.frame_start = self.hold_start
			pg.frame_end = self.hold_end
			pg.falloff_end = self.hold_falloff_end
			self.pressing = False
			bpy.types.SpaceGraphEditor.draw_handler_remove(self._handle, 'WINDOW')
			context.window.cursor_modal_restore()
			return {'CANCELLED'}

		return {'RUNNING_MODAL'}

	def invoke(self, context, event):
		area = bpy.context.area
		area.tag_redraw()
		pg = bpy.types.Scene.procedural_graph_handler
		#Get the right region, else it could run calculations on the wrong region inside the area
		region = None
		for r in area.regions:
			if r.type == "WINDOW":
				region = r
				break
		self.hover_frame,tmp = region.view2d.region_to_view(event.mouse_x - region.x, 0)
		self.hover_frame = round(self.hover_frame)

		if context.area.type == "GRAPH_EDITOR":
			self.hold_falloff_start = pg.falloff_start
			self.hold_start = pg.frame_start
			self.hold_end = pg.frame_end
			self.hold_falloff_end = pg.falloff_end
			if not pg.enabled:
				pg.enable()
			self._handle = bpy.types.SpaceGraphEditor.draw_handler_add(draw_selection_handle, (self, context), 'WINDOW', 'POST_PIXEL')
			context.window.cursor_modal_set("CROSSHAIR")
			context.window_manager.modal_handler_add(self)
			return {'RUNNING_MODAL'}
		else:
			self.report({'WARNING'}, "Somehow didn't run this in graph editor area")
			return {'CANCELLED'}
		
	
class GRAPH_OT_proceduralrange_clear(bpy.types.Operator):
	"""Clear range for procedural animation"""
	bl_idname = "graph.proceduralrange_clear"
	bl_label = "Clear Procedural Range"

	def invoke(self, context, event):
		if context.area.type =="GRAPH_EDITOR":
			bpy.types.Scene.procedural_graph_handler.disable()
			return {'FINISHED'}
		else:
			self.report({'WARNING'}, "Somehow didn't run this in graph editor area")
			return {'CANCELLED'}

class GRAPH_OT_proceduralrange_transform(bpy.types.Operator):
	"""Move the procedural selection."""
	bl_idname = "graph.proceduralrange_transform"
	bl_label = "Move Procedural Range"
	mouse_x_start: IntProperty()
	hold_start: IntProperty()
	#hold_end: IntProperty()
	selected_handle = None
	handle: EnumProperty(
		name="Handle",
		items=(
			('FALLOFF_START', "Left Falloff", ""),
			('FALLOFF_END', "Right Falloff", ""),
			('FRAME_START', "Left", ""),
			('FRAME_END', "Right", ""),
		),
	)
	
	def modal(self, context, event):
		#return {'RUNNING_MODAL'}
		area = bpy.context.area
		area.tag_redraw()
		pg = bpy.types.Scene.procedural_graph_handler
		region = None
		for r in area.regions:
			if r.type == "WINDOW":
				region = r
				break
		if event.type == 'MOUSEMOVE':
			##HACK: There has to be a better way to do this, since y is unused. 
			##  	May not need to tho, if add decorating lines.
			#minus by region's x to correct for the channel panel
			tmpx,tmpy = region.view2d.region_to_view(event.mouse_x - region.x, 0)
			distance = round(tmpx) - self.mouse_x_start
			if self.handle == "FALLOFF_START":
				pg.falloff_start = self.hold_start + distance
				if pg.falloff_start >= pg.frame_start:
					pg.falloff_start = pg.frame_start
			elif self.handle == "FALLOFF_END":
				pg.falloff_end = self.hold_start + distance
				if pg.falloff_end <= pg.frame_end:
					pg.falloff_end = pg.frame_end
			elif self.handle == "FRAME_START":
				pg.frame_start = self.hold_start + distance
				if pg.frame_start >= pg.frame_end:
					pg.frame_start = pg.frame_end
			elif self.handle == "FRAME_END":
				pg.frame_end = self.hold_start + distance
				if pg.frame_end <= pg.frame_start:
					pg.frame_end = pg.frame_start
				
			#pg.frame_end = self.hold_end + distance
		elif event.type == 'LEFTMOUSE':
			pg.update_range()
			context.window.cursor_modal_restore()
			return {'FINISHED'}
		elif event.type in {'RIGHTMOUSE', 'ESC'}:
			if self.handle == "FALLOFF_START":
				pg.falloff_start = self.hold_start
			elif self.handle == "FALLOFF_END":
				pg.falloff_end = self.hold_start
			elif self.handle == "FRAME_START":
				pg.frame_start = self.hold_start
			elif self.handle == "FRAME_END":
				pg.frame_end = self.hold_start
			pg.update_range()
			context.window.cursor_modal_restore()
			return {'CANCELLED'}
		return {'RUNNING_MODAL'}
	
	def invoke(self, context, event):
		pg = bpy.types.Scene.procedural_graph_handler
		region = None
		for r in bpy.context.area.regions:
			if r.type == "WINDOW":
				region = r
				break
		if context.area.type == "GRAPH_EDITOR":
			if self.handle == "FALLOFF_START":
				self.hold_start = pg.falloff_start
			elif self.handle == "FALLOFF_END":
				self.hold_start = pg.falloff_end
			elif self.handle == "FRAME_START":
				self.hold_start = pg.frame_start
			elif self.handle == "FRAME_END":
				self.hold_start = pg.frame_end
			tmpx, tmp = region.view2d.region_to_view(event.mouse_x - region.x, 0)
			self.mouse_x_start = round(tmpx)
			context.window.cursor_modal_set("SCROLL_X")
			context.window_manager.modal_handler_add(self)
			return {'RUNNING_MODAL'}
		else:
			self.report({'WARNING'}, "Somehow didn't run this in graph editor area")
			return {'CANCELLED'}
#endregion #RANGE MANIPULATION OPERATORS#
#region    #CURVE OPERATORS#
class GRAPH_OT_procedural_noise(bpy.types.Operator):
	"""Add noise to an FCurve"""
	bl_idname = "graph.procedural_noise"
	bl_label = "FCurve Procedural Noise"
	bl_options = {'REGISTER', 'UNDO'}
	
	fcurves = []
	ecurves = []
	Hfcurves = []
	rand_arr = []
	mouse_x_start : IntProperty()
	mouse_y_start : IntProperty()
	distance = 0
	step = 1
	strength = 0
	##TODO: Allow to type in distance amount, but keep calculated when unused
	
	def modal(self, context, event):
		bpy.context.area.tag_redraw()
		pg = bpy.types.Scene.procedural_graph_handler
		##TODO: add better tweaking via shift that uses area? current one should work for now tho.
		if event.type == 'MOUSEMOVE' or event.type == "LEFT_SHIFT":
			self.strength = (event.mouse_y - self.mouse_y_start) / (100, 1000)[event.shift]
			for i in range(0, len(self.fcurves)):
				fc = self.fcurves[i]
				ra = self.rand_arr[i]
				efc = self.ecurves[i]
				for frame_i in range(pg.start, pg.end+1):
					if frame_i % self.step != 0:
						continue
					fc.keyframe_points.insert(
						frame=frame_i, 
						value=scale(
									(self.distance * self.strength) * pg.get_frame_influence(frame_i), 
									efc[frame_i-pg.start], 
									efc[frame_i-pg.start] + ra[frame_i-pg.start]
									),
						options={"NEEDED"}, 
						keyframe_type="JITTER")
		elif event.type == 'WHEELUPMOUSE':
			self.step +=1
		elif event.type == 'WHEELDOWNMOUSE':
			self.step -=1
			if self.step < 1:
				self.step = 1
		elif event.type == 'LEFTMOUSE':
			self.clean(context)
			return {'FINISHED'}
		elif event.type in {'RIGHTMOUSE', 'ESC'}:
			restore_fcurves(self)
			self.clean(context)
			return {'CANCELLED'}
		context.area.header_text_set(text="Distance: %.3f, Strength: %.3f Step: %d" % (self.distance, self.strength, self.step))
		return {'RUNNING_MODAL'}
	def invoke(self, context, event):
		pg = bpy.types.Scene.procedural_graph_handler
		self.fcurves = get_active_fcurves(context)
		if handle_common_errors(self) == False:
			return {"CANCELLED"}
		for fc in self.fcurves:
			setup_fcurve_restore(self, fc)
			tmp_rand_arr = []
			tmp_arr = []
			for i in range(pg.start, pg.end+1):
				tmp_arr.append(fc.evaluate(i))
				if i == pg.start or i == pg.end:
					tmp_rand_arr.append(fc.evaluate(i))
					continue
				tmp_rand_arr.append(random.random()-0.5)
			self.rand_arr.append(tmp_rand_arr)
			self.ecurves.append(tmp_arr)

			if len(self.fcurves) == 0:
				self.report({'WARNING'}, "Must have Fcurve channel(s) selected.")
				return {'CANCELLED'}
			
			self.mouse_x_start, self.mouse_y_start = (event.mouse_x, event.mouse_y)
			region = None
		for r in context.area.regions:
			if r.type == "WINDOW":
				region = r
				break
		tmpx1,tmpy1 = region.view2d.region_to_view(0, region.height)
		tmpx2,tmpy2 = region.view2d.region_to_view(0, region.y) 		   
		self.distance = abs(tmpy1 - tmpy2)
			
		context.window.cursor_modal_set("SCROLL_Y")
		context.window_manager.modal_handler_add(self)
		return {'RUNNING_MODAL'}
	def clean(self, context):
		#Free array memory
		self.rand_arr.clear()
		self.fcurves.clear()
		self.Hfcurves.clear()
		self.ecurves.clear()
		self.fcurves.clear()
		#Fix UI things we modified
		context.area.header_text_set(text=None)
		context.window.cursor_modal_restore()
		
class GRAPH_OT_procedural_smooth(bpy.types.Operator):
	"""Apply Weighted Smooth on an FCurve"""
	bl_idname = "graph.procedural_smooth"
	bl_label = "FCurve Weighted Smooth"
	bl_options = {'REGISTER', 'UNDO'}
	
	Hfcurves = []
	fcurves = []
	ecurves = []
	wma_arr = []
	mouse_x_start : IntProperty()
	mouse_y_start : IntProperty()
	step = 1
	strength = 0
	clamped = True
	
	def modal(self, context, event):
		context.area.tag_redraw()
		pg = bpy.types.Scene.procedural_graph_handler
		
		if event.type == 'MOUSEMOVE' or event.type == "LEFT_SHIFT":
			self.strength = clamp((event.mouse_x - self.mouse_x_start) / (100, 1000)[event.shift]) if self.clamped else (event.mouse_x - self.mouse_x_start) / (100, 1000)[event.shift]
			for i in range(0, len(self.fcurves)):
				fc = self.fcurves[i]
				ra = self.wma_arr[i]
				efc = self.ecurves[i]
				for frame_index in range(pg.start, pg.end+1):
					if frame_index % self.step != 0:
						continue
					fc.keyframe_points.insert(
						frame=frame_index, 
						value=scale(
									self.strength * pg.get_frame_influence(frame_index), 
									efc[frame_index-pg.start][1], 
									ra[frame_index-pg.start]
									),
						options={"NEEDED"}, 
						keyframe_type="JITTER")
		elif event.type == 'WHEELUPMOUSE':
			self.step +=1
		elif event.type == 'WHEELDOWNMOUSE':
			self.step -=1
			if self.step < 1:
				self.step = 1
		elif event.type == 'C':
			if event.value == 'PRESS':
				self.clamped = not self.clamped
		elif event.type == 'LEFTMOUSE':
			self.clean(context)
			return {'FINISHED'}
		elif event.type in {'RIGHTMOUSE', 'ESC'}:
			restore_fcurves(self)
			self.clean(context)
			return {'CANCELLED'}
		context.area.header_text_set(text="Strength: %.3f Step: %d, [C]lamp: %s" % (self.strength, self.step, "ON" if self.clamped else "OFF"))
		return {'RUNNING_MODAL'}
	
	def invoke(self, context, event):
		area = context.area
		pg = bpy.types.Scene.procedural_graph_handler
		self.fcurves = get_active_fcurves(context)
		if handle_common_errors(self) == False:
			return {"CANCELLED"}

		for fc in self.fcurves:
			tmp_arr = []
			ev_arr = []
			setup_fcurve_restore(self, fc)
			for frame_index in range(pg.start, pg.end+1):
				ev_arr.append((frame_index, fc.evaluate(frame_index)))
				if frame_index == pg.start or frame_index == pg.end:
					tmp_arr.append(fc.evaluate(frame_index))
					continue
				ef = [
					fc.evaluate(frame_index-2), 
					fc.evaluate(frame_index-1), 
					fc.evaluate(frame_index),
					fc.evaluate(frame_index+1),
					fc.evaluate(frame_index+2)
				]
				#tmp_arr.append(calc_weight_three_point(ef[0], ef[1], ef[2]))
				tmp_arr.append(calc_weight_five_point(ef[0], ef[1], ef[2], ef[3], ef[4]))
			self.ecurves.append(ev_arr)
			self.wma_arr.append(tmp_arr)
		self.mouse_x_start, self.mouse_y_start = (event.mouse_x, event.mouse_y)
		context.window.cursor_modal_set("SCROLL_X")
		context.window_manager.modal_handler_add(self)
		return {'RUNNING_MODAL'}

	def clean(self, context):
		#Free up array memory
		self.fcurves.clear()
		self.Hfcurves.clear()
		self.ecurves.clear()
		self.wma_arr.clear()
		#Fix UI things we modified
		context.area.header_text_set(text=None)
		context.window.cursor_modal_restore()

class GRAPH_OT_procedural_ramp(bpy.types.Operator):
	"""Modifies FCurve to go from selection start to finish linearly."""
	bl_idname = "graph.procedural_ramp"
	bl_label = "FCurve Ramp"
	bl_options = {'REGISTER', 'UNDO'}
	
	Hfcurves = []
	fcurves = []
	ecurves = []
	rmp_arr = []
	mouse_x_start : IntProperty()
	mouse_y_start : IntProperty()
	step = 1
	strength = 0
	clamped = True
	
	def modal(self, context, event):
		context.area.tag_redraw()
		pg = bpy.types.Scene.procedural_graph_handler
		if event.type == 'MOUSEMOVE' or event.type == "LEFT_SHIFT":
			self.strength = clamp((event.mouse_x - self.mouse_x_start) / (100, 1000)[event.shift]) if self.clamped else (event.mouse_x - self.mouse_x_start) / (100, 1000)[event.shift]
			for i in range(0, len(self.fcurves)):
				fc = self.fcurves[i]
				ra = self.rmp_arr[i]
				efc = self.ecurves[i]
				for frame_index in range(pg.start, pg.end+1):
					if frame_index % self.step != 0:
						continue
					fc.keyframe_points.insert(
						frame=frame_index, 
						value=scale(
									self.strength * pg.get_frame_influence(frame_index), 
									efc[frame_index-pg.start][1], 
									ra[frame_index-pg.start]
									),
						options={"NEEDED"}, 
						keyframe_type="JITTER")
		elif event.type == 'WHEELUPMOUSE':
			self.step +=1
		elif event.type == 'WHEELDOWNMOUSE':
			self.step -=1
			if self.step < 1:
				self.step = 1
		elif event.type == 'C':
			if event.value == 'PRESS':
				self.clamped = not self.clamped
		elif event.type == 'LEFTMOUSE':
			self.clean(context)
			return {'FINISHED'}
		elif event.type in {'RIGHTMOUSE', 'ESC'}:
			restore_fcurves(self)
			self.clean(context)
			return {'CANCELLED'}
		context.area.header_text_set(text="Strength: %.3f Step: %d, [C]lamp: %s" % (self.strength, self.step, "ON" if self.clamped else "OFF"))
		return {'RUNNING_MODAL'}
	def invoke(self, context, event):
		pg = bpy.types.Scene.procedural_graph_handler
		self.fcurves = get_active_fcurves(context)
		if handle_common_errors(self) == False:
			return {"CANCELLED"}

		for fc in self.fcurves:
			tmp_arr = []
			ev_arr = []
			setup_fcurve_restore(self, fc)
			for frame_index in range(pg.start, pg.end+1):
				ev_arr.append((frame_index, fc.evaluate(frame_index)))
				if frame_index == pg.start or frame_index == pg.end:
					tmp_arr.append(fc.evaluate(frame_index))
					continue
				a, b, s = (None, None, None)
				if frame_index < pg.frame_start:
					a = fc.evaluate(pg.falloff_start)
					b = fc.evaluate(pg.frame_start)
					s = normalize(frame_index, pg.falloff_start, pg.frame_start)
				elif frame_index > pg.frame_end:
					a = fc.evaluate(pg.frame_end)
					b = fc.evaluate(pg.falloff_end)
					s = normalize(frame_index, pg.frame_end, pg.falloff_end)
				else:
					a = fc.evaluate(pg.frame_start)
					b = fc.evaluate(pg.frame_end)
					s = normalize(frame_index, pg.frame_start, pg.frame_end)
				val = scale(s,a,b)
				tmp_arr.append(val)
			self.ecurves.append(ev_arr)
			self.rmp_arr.append(tmp_arr)
		self.mouse_x_start, self.mouse_y_start = (event.mouse_x, event.mouse_y)
		context.window.cursor_modal_set("SCROLL_X")
		context.window_manager.modal_handler_add(self)
		return {'RUNNING_MODAL'}

	def clean(self, context):
		#Free up array memory
		self.fcurves.clear()
		self.Hfcurves.clear()
		self.ecurves.clear()
		self.rmp_arr.clear()
		#Fix UI things we modified
		context.area.header_text_set(text=None)
		context.window.cursor_modal_restore()

class GRAPH_OT_procedural_cursor(bpy.types.Operator):
	"""Modifies the Procedural range selection to move towards the cursor's value."""
	bl_idname = "graph.procedural_cursor"
	bl_label = "FCurve Cursor"
	bl_options = {'REGISTER', 'UNDO'}
	
	Hfcurves = []
	fcurves = []
	ecurves = []
	value : FloatProperty(default=0)
	type : StringProperty(default='')
	mouse_x_start : IntProperty()
	mouse_y_start : IntProperty()
	step = 1
	strength = 0
	clamped = True
	
	def modal(self, context, event):
		context.area.tag_redraw()
		pg = bpy.types.Scene.procedural_graph_handler
		if event.type == 'MOUSEMOVE' or event.type == "LEFT_SHIFT":
			self.strength = clamp((event.mouse_x - self.mouse_x_start) / (100, 1000)[event.shift]) if self.clamped else (event.mouse_x - self.mouse_x_start) / (100, 1000)[event.shift]
			for i in range(0, len(self.fcurves)):
				fc = self.fcurves[i]
				efc = self.ecurves[i]
				for frame_index in range(pg.start, pg.end+1):
					if frame_index % self.step != 0:
						continue
					fc.keyframe_points.insert(
						frame=frame_index, 
						value=scale(
									self.strength * pg.get_frame_influence(frame_index), 
									efc[frame_index-pg.start][1], 
									self.value
									),
						options={"NEEDED"}, 
						keyframe_type="JITTER")
		elif event.type == 'WHEELUPMOUSE':
			self.step +=1
		elif event.type == 'WHEELDOWNMOUSE':
			self.step -=1
			if self.step < 1:
				self.step = 1
		elif event.type == 'C':
			if event.value == 'PRESS':
				self.clamped = not self.clamped
		elif event.type == 'LEFTMOUSE':
			self.clean(context)
			return {'FINISHED'}
		elif event.type in {'RIGHTMOUSE', 'ESC'}:
			restore_fcurves(self)
			self.clean(context)
			return {'CANCELLED'}
		context.area.header_text_set(text="Strength: %.3f Step: %d, [C]lamp: %s" % (self.strength, self.step, "ON" if self.clamped else "OFF"))
		return {'RUNNING_MODAL'}
	def invoke(self, context, event):
		pg = bpy.types.Scene.procedural_graph_handler
		self.fcurves = get_active_fcurves(context)
		if handle_common_errors(self) == False:
			return {"CANCELLED"}

		##Prob use enum for this
		if self.type == "CURSOR":
			for space in bpy.context.area.spaces:
				if space.type == "GRAPH_EDITOR":
					self.value = space.cursor_position_y
		elif self.type == "PLAYHEAD":
			self.value = context.active_editable_fcurve.evaluate(context.scene.frame_current)
		elif self.type == "ZERO":
			self.value = 0
		else:
			self.report({'ERROR'}, 'type must be ["CURSOR", "PLAYHEAD", "ZERO"]')
			return {"CANCELLED"}

		for fc in self.fcurves:
			ev_arr = []
			setup_fcurve_restore(self, fc)
			for frame_index in range(pg.start, pg.end+1):
				ev_arr.append((frame_index, fc.evaluate(frame_index)))
			self.ecurves.append(ev_arr)
		self.mouse_x_start, self.mouse_y_start = (event.mouse_x, event.mouse_y)
		context.window.cursor_modal_set("SCROLL_X")
		context.window_manager.modal_handler_add(self)
		return {'RUNNING_MODAL'}

	def clean(self, context):
		#Free up array memory
		self.fcurves.clear()
		self.Hfcurves.clear()
		self.ecurves.clear()
		self.playhead_value = 0
		#Fix UI things we modified
		context.area.header_text_set(text=None)
		context.window.cursor_modal_restore()
#endregion #CURVE OPERATORS#
##TODO: Use this as a modal for the strength/phase offset as well?
class GRAPH_OT_procedural_range_to_modifier(bpy.types.Operator):
	"""Adds or changes an Fcurve modifier with the frame range and falloff range of the selected procedural range"""
	bl_idname = "graph.procedural_range_to_modifier"
	bl_label = "Procedural Range to Modifier"
	bl_options = {'REGISTER', 'UNDO'}
	#TODO: Probably use an Enum for this?
	modifier : StringProperty(name="")
	fcurves = []
	def invoke(self, context, event):
		self.fcurves = get_active_fcurves(context)
		if handle_common_errors(self) == None:
			return {'CANCELLED'}

		mods = []
		if self.modifier == 'ACTIVE':
			mods.append(bpy.context.active_editable_fcurve.modifiers.active)
		else:
			for fcurve in self.fcurves:
				if self.modifier == 'GENERATOR':
					mods.append(fcurve.modifiers.new(type='GENERATOR'))
				elif self.modifier == 'FNGENERATOR':
					mods.append(fcurve.modifiers.new(type='FNGENERATOR'))
				elif self.modifier == 'ENVELOPE':
					mods.append(fcurve.modifiers.new(type='ENVELOPE'))
				elif self.modifier == 'CYCLES':
					mods.append(fcurve.modifiers.new(type='CYCLES'))
				elif self.modifier == 'NOISE':
					mods.append(fcurve.modifiers.new(type='NOISE'))
				elif self.modifier == 'LIMITS':
					mods.append(fcurve.modifiers.new(type='LIMITS'))
				elif self.modifier == 'STEPPED':
					mods.append(fcurve.modifiers.new(type='STEPPED'))
		
		pg = bpy.types.Scene.procedural_graph_handler
		for mod in mods:
			mod.use_restricted_range = True
			mod.frame_start = pg.start
			mod.frame_end = pg.end
			mod.blend_in = pg.frame_start - pg.falloff_start
			mod.blend_out = pg.falloff_end - pg.frame_end
			
		self.fcurves.clear()
		return {'FINISHED'}

#region    #Menus#
class GRAPH_MT_procedural_fcurves(bpy.types.Menu):
	bl_label = ""
	bl_idname = "GRAPH_MT_procedural_fcurves"

	def draw(self, context):
		layout = self.layout
		row = layout.row()
		col = row.column()
		col.operator_context = "INVOKE_DEFAULT"

		col.label(text="Fcurve Manipulation")
		col.separator()
		col.operator("graph.procedural_cursor", text="Cursor", icon="CURSOR")['type'] = 'CURSOR'
		col.operator("graph.procedural_cursor", text="Playhead", icon="PLAY")['type'] = 'PLAYHEAD'
		col.operator("graph.procedural_cursor", text="Zero", icon="IMPORT")['type'] = 'ZERO'
		col.operator("graph.procedural_noise", text="Noise", icon="MOD_NOISE")
		col.operator("graph.procedural_smooth", text="WMA Smooth", icon="MOD_SMOOTH")
		col.operator("graph.procedural_ramp", text="Ramp", icon="IPO_LINEAR")

		# Modifier Col
		col = row.column()
		col.label(text="Modifier Range")
		col.separator()
		col.operator("graph.procedural_range_to_modifier", text="Active", icon="LAYER_ACTIVE")['modifier'] = 'ACTIVE'
		#col.operator("graph.procedural_range_to_modifier", text="All")
		col.operator("graph.procedural_range_to_modifier", text="Generator", icon="DRIVER_TRANSFORM")['modifier'] = 'GENERATOR'
		col.operator("graph.procedural_range_to_modifier", text="Built-In Function", icon="DRIVER_TRANSFORM")['modifier'] = 'FNGENERATOR'
		col.operator("graph.procedural_range_to_modifier", text="Envelope", icon="CURVE_DATA")['modifier'] = 'ENVELOPE'
		col.operator("graph.procedural_range_to_modifier", text="Cycles", icon="FILE_REFRESH")['modifier'] = 'CYCLES'
		col.operator("graph.procedural_range_to_modifier", text="Noise", icon="MOD_NOISE")['modifier'] = 'NOISE'
		col.operator("graph.procedural_range_to_modifier", text="Limits", icon="CON_DISTLIMIT")['modifier'] = 'LIMITS'
		col.operator("graph.procedural_range_to_modifier", text="Stepped Interpolation", icon="IPO_CONSTANT")['modifier'] = 'STEPPED'

class GRAPH_MT_PIE_procedural_transform(bpy.types.Menu):
	# label is displayed at the center of the pie menu.
	bl_label = "Select Handle"

	def draw(self, context):
		layout = self.layout

		pie = layout.menu_pie()
		# operator_enum will just spread all available options
		# for the type enum of the operator on the pie
		pie.operator_enum("graph.proceduralrange_transform", "handle")
#endregion #Menus#
#region    #Registration#
classes = [
	GRAPH_OT_proceduralrange_set,
	GRAPH_OT_proceduralrange_clear,
	GRAPH_OT_proceduralrange_transform,
	GRAPH_OT_procedural_noise,
	GRAPH_OT_procedural_smooth,
	GRAPH_OT_procedural_ramp,
	GRAPH_OT_procedural_cursor,
	GRAPH_OT_procedural_range_to_modifier,
	GRAPH_MT_procedural_fcurves,
	GRAPH_MT_PIE_procedural_transform,
]

addon_keymaps = []

def register():
	for cls in classes:
		bpy.utils.register_class(cls)
	bpy.types.Scene.procedural_graph_handler = Procedural_Graph_Handler()
	#bpy.types.Scene.procedural_graph_settings = ProceduralGraphSettings(bpy.data.rna_type)

	##Add shortcuts
	wm = bpy.context.window_manager
	kc = wm.keyconfigs.addon
	if kc:
		km = kc.keymaps.new(name='Graph Editor Generic', space_type='GRAPH_EDITOR')
		kmi = km.keymap_items.new("graph.proceduralrange_set", type='Z', value='PRESS')
		addon_keymaps.append((km, kmi))
		kmi = km.keymap_items.new("wm.call_menu", type='C', value='PRESS', shift=True)
		kmi.properties.name = "GRAPH_MT_procedural_fcurves"
		addon_keymaps.append((km, kmi))
		kmi = km.keymap_items.new("graph.proceduralrange_clear", type='Z', value='PRESS', alt=True)
		addon_keymaps.append((km, kmi))
		kmi = km.keymap_items.new("wm.call_menu_pie", type='G', value='PRESS', shift=True)
		kmi.properties.name = "GRAPH_MT_PIE_procedural_transform"
		addon_keymaps.append((km, kmi))

	
def unregister():
	#print("Unregistering Procedural Graph")
	bpy.types.Scene.procedural_graph_handler.disable()
	del bpy.types.Scene.procedural_graph_handler
	for cls in classes:
		bpy.utils.unregister_class(cls)

	for km, kmi in addon_keymaps:
		km.keymap_items.remove(kmi)
	addon_keymaps.clear()

#endregion #Registration#

if __name__ == "__main__":
	try:
		unregister()
	except RuntimeError as e:
		print(e)
	except AttributeError as e:
		print(e)
	except:
		import sys
		print("Error:", sys.exc_info()[0])
		pass
	register()